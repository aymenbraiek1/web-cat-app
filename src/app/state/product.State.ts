
//une structre que je developé pour utliser dans le produit component 
export enum DataStateEnum
{
    LOADING,LOADED,ERROR
}
//declarer mon state ici type génerique 
export interface AppDataState<T>{
    //de type DataStateEnum 
    dataState?:DataStateEnum,
    //data pourra etre liste de produits ou bien autre chose 
    data?:T,
//message error de type string
    errorMessage?:string;
    


}

export enum ProductActionType{
    GET_ALL_PRODUCTS="[Product] Get All Products",
    GET_SELECTED_PRODUCTS="[Product] Get Selected Products",
    SEARCH_PRODUCTS="[Product] Get All Products",
    GET_AVAILABLE_PRODUCTS="[Product] Search Products",
    NEW_PRODUCTS="[Product] NEW  Products",
    SELECT_PRODUCTS="[Product] SELECT  Products",
    EDIT_PRODUCTS="[Product] EDIT  Products",
    DELETE_PRODUCTS="[Product] DELETE   Products",
    PRODUCT_ADDED="[Product]  Products addes",
    PRODUCT_UPDATED="[Product] Products updates"
}

export interface ActionEvent
{
    //generalement est defini par deux types ProductActionType et payload si je connais que toutes les paramétres de type string 
    //sinon en definit any 
    type:ProductActionType,
    payload?:any,
}