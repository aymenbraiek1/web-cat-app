import { ActionEvent } from './product.State';
import { Subject } from 'rxjs';
import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export class EventDriverService {

    //objectif est de centraliser la gestion des évenements dans un seul 
    //object 
    //sourceEventSubject dans cela on va publier des messages de type ActionEvent
    //autre component veut recevoir ce event il suufit qu'il fasse apel sourceEventSubjectObservable.subscribe
    sourceEventSubject: Subject<ActionEvent> = new Subject<ActionEvent>();
    sourceEventSubjectObservable = this.sourceEventSubject.asObservable();
    


    //definir une methode =lorsque je fais apel à cette methodes tous simplement publier ce event dans sourceEventSubject
    //toutes les conponents qui font un subscribe dans ce subject vont recevoir l'évenement
    //ce traitement est similaire au JMS ou bien kafka ...
    //pour transferer un evenement d'un component à des autres componenets il suffit de fait apel à la methodes publishEvent
    publishEvent(event: ActionEvent) {
        //.next cda publier un évenement 
        this.sourceEventSubject.next(event);
    }
}