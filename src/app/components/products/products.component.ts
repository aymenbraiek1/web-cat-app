import { EventDriverService } from './../../state/event.driver.service';
import { ActionEvent } from './../../state/product.State';
import { Product } from './../../model/product.model';
import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ProductsService } from 'src/app/services/products.service';
import { AppDataState, DataStateEnum, ProductActionType } from 'src/app/state/product.State';
import { map, catchError, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  // ? que le products pourra un valeur par defaut(la valuer est obsolete ) version 11 /2 syntaxe est utliser soit 
  // products?:Product[];
  products$: Observable<AppDataState<Product[]>> | null = null;

  //creer un varibale utliser dans ngSwithCase
  readonly DataStateEnum = DataStateEnum;

  //injecter le sercies 
  //injecter EventDriverService
  constructor(private productsService: ProductsService, private router: Router, private eventDriverService: EventDriverService) { }

  ngOnInit(): void {
    //dés que le component demarre va appel
    //lorsque le component demarre ecoute moi ce que ce passe sur sourceEventSubjectObservable(broker)
    this.eventDriverService.sourceEventSubjectObservable.subscribe((actionEvent: ActionEvent) => {
      this.onActionEvent(actionEvent);
      console.log(actionEvent);
    })
  }

  onGetAvailableproducts() {

    this.products$ = this.productsService.getAvailableProducts()

      .pipe(map(data => ({ dataState: DataStateEnum.LOADED, data: data })),
        startWith({ dataState: DataStateEnum.LOADING }),
        catchError(err => of({ dataState: DataStateEnum.ERROR, errorMessage: err.message })));

  }

  onGetSelectedproducts() {
    this.products$ = this.productsService.getSelectedProducts()

      .pipe(map(data => ({ dataState: DataStateEnum.LOADED, data: data })),
        startWith({ dataState: DataStateEnum.LOADING }),
        catchError(err => of({ dataState: DataStateEnum.ERROR, errorMessage: err.message })));
  }

  onGetAllproducts() {
    //
    this.products$ = this.productsService.getAllProducts()
      //je return que le donnés
      //1 ligne si le valeur de retour est liste des produits
      // avant le retour de req il affiche un traitement
      // si en cas d'error afficher le msg de error 
      .pipe(map(data => ({ dataState: DataStateEnum.LOADED, data: data })),
        startWith({ dataState: DataStateEnum.LOADING }),
        catchError(err => of({ dataState: DataStateEnum.ERROR, errorMessage: err.message })));


  }

  onSearch(dataForm: any) {
    console.log(dataForm.keyword);
    this.products$ = this.productsService.searchProduct(dataForm.keyword)

      .pipe(map(data => ({ dataState: DataStateEnum.LOADED, data: data })),
        startWith({ dataState: DataStateEnum.LOADING }),
        catchError(err => of({ dataState: DataStateEnum.ERROR, errorMessage: err.message })));
  }

  onSelect(p: Product) {
    this.productsService.select(p).subscribe(data => { p.selected = data.selected; }
    )
  }

  onDelete(product: Product)
  //charger les donnés apres la supprission
  {
    let v = confirm("Etes vous sure de suprimer ce element");
    if (v == true)
      this.productsService.deleteProduct(product).subscribe(data => {
        this.onGetAllproducts();
      })
  }
  onNewproducts() {
    this.router.navigateByUrl("/newProduct");
  }


  onEdit(p: Product) {
    this.router.navigateByUrl("/editProduct/" + p.id);
  }

  onActionEvent($event: ActionEvent) {
    switch ($event.type) {
      case ProductActionType.GET_ALL_PRODUCTS: this.onGetAllproducts(); break;
      case ProductActionType.GET_SELECTED_PRODUCTS: this.onGetSelectedproducts(); break;
      case ProductActionType.GET_AVAILABLE_PRODUCTS: this.onGetAvailableproducts(); break;
      case ProductActionType.SEARCH_PRODUCTS: this.onSearch($event.payload); break;
      case ProductActionType.NEW_PRODUCTS: this.onNewproducts(); break;
      case ProductActionType.SELECT_PRODUCTS: this.onSelect($event.payload); break;
      case ProductActionType.EDIT_PRODUCTS: this.onEdit($event.payload); break;
      case ProductActionType.DELETE_PRODUCTS: this.onDelete($event.payload); break;
    }
  }
}

