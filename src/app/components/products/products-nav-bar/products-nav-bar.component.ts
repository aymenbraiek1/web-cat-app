import { EventDriverService } from './../../../state/event.driver.service';
import { ActionEvent } from './../../../state/product.State';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ProductActionType } from 'src/app/state/product.State';

@Component({
  selector: 'app-products-nav-bar',
  templateUrl: './products-nav-bar.component.html',
  styleUrls: ['./products-nav-bar.component.css']
})
export class ProductsNavBarComponent implements OnInit {
  //@Output() producteventEmiter: EventEmitter<ActionEvent> = new EventEmitter();
  constructor(private eventDriverService: EventDriverService) { }

  ngOnInit(): void {
  }
  onGetAllproducts() {
    //la methode de child vers le parent est n'est pas avec paramétre donc 
    //payload est n'est pas obligatoire à remplir 
    //this.producteventEmiter.emit({type:ProductActionType.GET_ALL_PRODUCTS});
    this.eventDriverService.publishEvent({ type: ProductActionType.GET_ALL_PRODUCTS });

  }
  onGetSelectedproducts() {
    //this.producteventEmiter.emit({type:ProductActionType.GET_SELECTED_PRODUCTS});
    this.eventDriverService.publishEvent({ type: ProductActionType.GET_SELECTED_PRODUCTS });
  }




  onGetAvailableproducts() {
    // this.producteventEmiter.emit({ type: ProductActionType.GET_AVAILABLE_PRODUCTS });
    this.eventDriverService.publishEvent({ type: ProductActionType.GET_AVAILABLE_PRODUCTS });
  }

  onNewproducts() {
    // this.producteventEmiter.emit({ type: ProductActionType.NEW_PRODUCTS })
    this.eventDriverService.publishEvent({ type: ProductActionType.NEW_PRODUCTS });
  }

  onSearch(dataForm: any) {
    //this.producteventEmiter.emit({ type: ProductActionType.SEARCH_PRODUCTS, payload: dataForm })
    this.eventDriverService.publishEvent({ type: ProductActionType.SEARCH_PRODUCTS, payload: dataForm });
  }

}
