import { EventDriverService } from './../../../../state/event.driver.service';
import { Observable } from 'rxjs';
import { Product } from './../../../../model/product.model';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ActionEvent, AppDataState, ProductActionType } from 'src/app/state/product.State';

@Component({
  selector: 'app-products-item',
  templateUrl: './products-item.component.html',
  styleUrls: ['./products-item.component.css']
})
export class ProductsItemComponent implements OnInit {
  @Input() prod: Product | null = null;
  //@Output() ItemeventEmitter: EventEmitter<ActionEvent> = new EventEmitter();


  constructor(private eventDriverService: EventDriverService) { }

  ngOnInit(): void {
  }

  onSelect(product: Product) {
    //this.ItemeventEmitter.emit({type:ProductActionType.SELECT_PRODUCTS,payload:product});
    this.eventDriverService.publishEvent({ type: ProductActionType.SELECT_PRODUCTS, payload: product });
  }

  onDelete(product: Product) {
    //this.ItemeventEmitter.emit({type:ProductActionType.DELETE_PRODUCTS,payload:product});
    this.eventDriverService.publishEvent({ type: ProductActionType.DELETE_PRODUCTS, payload: product });
  }

  onEdit(product: Product) {
    //this.ItemeventEmitter.emit({type:ProductActionType.EDIT_PRODUCTS,payload:product});
    this.eventDriverService.publishEvent({ type: ProductActionType.EDIT_PRODUCTS, payload: product });

  }

}
