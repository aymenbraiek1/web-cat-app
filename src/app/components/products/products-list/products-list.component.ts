import { EventDriverService } from './../../../state/event.driver.service';
import { Product } from './../../../model/product.model';
import { Observable } from 'rxjs';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActionEvent, AppDataState, DataStateEnum, ProductActionType } from 'src/app/state/product.State';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
 @Input() productsinput$:Observable<AppDataState<Product[]>> | null=null;
 //@Output() productsEventEmitter:EventEmitter<ActionEvent>=new EventEmitter();
  //creer un varibale utliser dans ngSwithCase
  readonly DataStateEnum=DataStateEnum;
  constructor(private eventDriverService:EventDriverService) { }

  ngOnInit(): void {
   
  }
  /*onSelect(p:Product)
  {

//this.productsEventEmitter.emit({type:ProductActionType.SELECT_PRODUCTS,payload:p});
  }
  onDelete(p:Product)
  {
this.productsEventEmitter.emit({type:ProductActionType.DELETE_PRODUCTS,payload:p});
  }

  onEdit(p:Product)
  {
    this.productsEventEmitter.emit({type:ProductActionType.EDIT_PRODUCTS,payload:p})

  }
  onActionEvent($event:ActionEvent)
{
  
      //puisque cette methode event est défini dans le parent product component il suffit que faire le traitement 
      //suivant  il va remonter 
      this.productsEventEmitter.emit($event);
       
}*/

  }

