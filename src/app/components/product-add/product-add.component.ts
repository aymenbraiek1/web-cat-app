import { EventDriverService } from './../../state/event.driver.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';
import { ProductActionType } from 'src/app/state/product.State';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {
//  ? indique que je suis pas obligé de définir les valeurs par defaut 
  productformGroup?:FormGroup;
  submitted : boolean=false;

  constructor(private fb:FormBuilder,private productservice:ProductsService,private eventDriveService:EventDriverService) { }

  ngOnInit(): void {
    // initialiser la formGroup +utliser les validators (valider le chamo ) 
    //["",validators.required ] =le valeur par defaut est null +valider le champ
    this.productformGroup=this.fb.group({
      name:["",Validators.required],
      price:[0,Validators.required],
      quantity:[0,Validators.required],
      selected:[true,Validators.required],
      available:[true,Validators.required]
    });}

    OnSaveProduct()
    {
this.submitted=true;
console.log(this.submitted);
if(this.productformGroup?.invalid) return ;
      this.productservice.save(this.productformGroup?.value).subscribe(data=>{
        this.eventDriveService.publishEvent({type:ProductActionType.PRODUCT_ADDED});
        alert("opération est effectué avec succes")
      });
    }
  }
