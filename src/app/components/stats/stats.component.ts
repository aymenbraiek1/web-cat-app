import { ActionEvent } from './../../state/product.State';
import { EventDriverService } from './../../state/event.driver.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {
counter:number=0;
  constructor(private eventDriveService:EventDriverService) { }

  ngOnInit(): void {
    this.eventDriveService.sourceEventSubjectObservable.subscribe((actionEvent:ActionEvent)=>{
      ++this.counter;
    })
  }

}
