import { EventDriverService } from './../../state/event.driver.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductActionType } from 'src/app/state/product.State';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  productId:number;
  productFormGroup?:FormGroup;
  submitted:boolean=false;

  constructor(private eventDriveService:EventDriverService,private activatedRoute:ActivatedRoute,private productservice:ProductsService,private form:FormBuilder, private router:Router) {
    this.productId=activatedRoute.snapshot.params.id;
   }

  ngOnInit(): void {
    this.productservice.getProducts(this.productId)
    .subscribe(product=> {
      this.productFormGroup=this.form.group({
        id:[product.id,Validators.required],
        name:[product.name,Validators.required],
        price:[product.price,Validators.required],
        quantity:[product.quantity,Validators.required],
        selected:[product.selected,Validators.required],
        available:[product.available,Validators.required]
      })

    });
  }
  OnUpdateProduct(){
    this.productservice.UpdateProducts(this.productFormGroup?.value).subscribe(data=>{
      this.eventDriveService.publishEvent({type:ProductActionType.PRODUCT_UPDATED});
alert("update est effectué avec succes ");


    });
    
  }
}
