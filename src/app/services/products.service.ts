import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../model/product.model';
//Injectable this service is enabled to root racine
@Injectable({
  providedIn: 'root'
})
export class ProductsService {
// Dependency Injection http
  constructor(private http:HttpClient) { }


  //getall products to service
  //on sait que notre methodes et retouner un objet de type observable contient liste de produits
  getAllProducts():Observable<Product[]>
  {
    let host=environment.host;
    //methode get declarer le type => j attend une liste des produits 
    return this.http.get<Product[]>(host+"/products")
}

//method qui definit que les produits selected 
getSelectedProducts():Observable<Product[]>
  {
    let host=environment.host;
    return this.http.get<Product[]>(host+"/products?selected=true");
}

//method qui definit que les produits selected 
getAvailableProducts():Observable<Product[]>
  {
    let host=environment.host;
    return this.http.get<Product[]>(host+"/products?available=true");
}

searchProduct(keyword:string):Observable<Product[]>
{
  let host=environment.host;

  return this.http.get<Product[]>(host+"/products?name_like="+keyword);
}
select(product:Product):Observable<Product>
{
  let host=environment.host;
product.selected=!product.selected;
  return this.http.put<Product>(host+"/products/"+product.id,product);
  //payload ????
}

deleteProduct(product:Product):Observable<void>
{
  let host=environment.host;
  return this.http.delete<void>(host+"/products/"+product.id);
}
save(product:Product):Observable<Product>
{
  let host=environment.host;
product.selected=!product.selected;
  return this.http.post<Product>(host+"/products/",product);
  //payload ????
}

Edit(product:Product):Observable<Product>
{
  let host=environment.host;
product.selected=!product.selected;
  return this.http.put<Product>(host+"/products/"+product.id,product);
  //payload ????
}

getProducts(id:number):Observable<Product>
  {
    let host=environment.host;
    return this.http.get<Product>(host+"/products/"+id);
}

UpdateProducts(p:Product):Observable<Product>
  {
    let host=environment.host;
    return this.http.put<Product>(host+"/products/"+p.id,p);
}
}



